"use strict";
// import * as whitemoves from "./whitemoves.js"; //{pawn} from "./whitemoves.js";
// import * as blackmoves from "./blackmoves.js";
var chessBoard; //= document.getElementById("chessBoard");
var gameTable; //= document.getElementById("gameTable");
var squares; //= [];
//var whitePieces, blackPieces;
//var sprites;
var turn;
var turnIndicator;
var promotionDialog;
var promotionForm;
var promotionOptions = ["queen", "rook", "bishop", "knight"];
var forPromotion;
//var lastMove;
var whiteKingPos = 60,
  blackKingPos = 4,
  previousPosition = 0,
  tempSprite; //to check for check & en passant
//tempsprite was originally local variable for moving sprites. lastmovedsquare would maybe be a better name
//check for self-check and check before end-turn & break out of function if true
var opponent;
var selectedSprite;
var enPassant = false;
var castle = false;

window.onload = function () {
  chessBoard = document.getElementById("chessBoard");
  gameTable = document.getElementById("gameTable");
  //sprites = document.getElementById("sprites");
  squares = [];
  createChessBoard();
};

//issue:
//if x piece moves and king is then in check, should be illegal move
//solution:
//make an "endTurn" function that checks before actually -- see check function

//bugs:
//capturing pieces on the edge of a square will put the two pieces in the same square -
//leading to some unexpected behavior -
//a combined piece will spit out the captured piece on the next move, as this is the first child.

//movement:
//want images to be draggable
//not sure this works with my current solution, try dragging td-s instead maybe
//also want sprites to be clickable, highlighting possible movements.
function highlightMoves(event) {
  // OMGaaahd should've made functions that take inn all these as factors as params
  //check if any squares are focused
  //----------------------------------------capture or move piece until next long line
  var targetSprite = parseInt(event.target.parentNode.id.split("-")[1]); //- only works for sprited squares
  if (selectedSprite !== undefined) {
    // is defined when moves are shown
    if (squares[targetSprite] === undefined) {
      //can't take sprites as sprite is clicked, not square... need 2 separate logics for empty and taken squares
      var targetSquare = parseInt(event.target.id.split("-")[1]);
      //check if target square is focused -- true = move, false = unfocus all
      if (squares[targetSquare].classList.contains("focusedSquare")) {
        //move selectedSprite to targetSquare
				tempSprite = squares[selectedSprite].childNodes[0];
				var castleRook;
				if (enPassant === true && selectedSprite-targetSquare !== 8 && squares[selectedSprite].childNodes[0].classList.contains("pawn") && squares[selectedSprite].childNodes[0].classList.contains("black")) {
					squares[targetSquare-8].childNodes[0].remove();
				}
				else if(enPassant === true && selectedSprite-targetSquare !== -8 && squares[selectedSprite].childNodes[0].classList.contains("pawn") && squares[selectedSprite].childNodes[0].classList.contains("white")){
					squares[targetSquare - -8].childNodes[0].remove();
				}
				if (castle === true && selectedSprite === targetSquare+2){
					castleRook = squares[targetSquare-1].childNodes[0];
					squares[targetSquare-1].childNodes[0].remove();
					squares[targetSquare+1].appendChild(castleRook);
				}
				else if (castle === true && selectedSprite === targetSquare-2){
					castleRook = squares[targetSquare+2].childNodes[0];
					squares[targetSquare+2].childNodes[0].remove();
					squares[targetSquare-1].appendChild(castleRook);
				}
				squares[selectedSprite].childNodes[0].remove();
				if(squares[targetSquare].childNodes[0] !== undefined){ //bugfix for capturing the border
					squares[targetSquare].childNodes[0].remove();
				}
				squares[targetSquare].appendChild(tempSprite);
        setMoved(targetSquare);
        if(squares[targetSquare].childNodes[0].classList.contains("pawn")
        && (targetSquare > 56 && turn === "black" || targetSquare < 9 && turn === "white") ){
          promote(turn, targetSquare);
        }
        // if(squares[targetSquare].childNodes[0].)
        //change turn after move is made -- make endturn func mby, and move to there
        switch (
          turn //used twice... should've been in a function
        ) {
          case "white":
            turn = "black";
            opponent = "white";
            break;
          case "black":
            turn = "white";
            opponent = "black";
            break;
        }
        turnIndicator.style.backgroundColor = turn;
      }
    } else if (
      squares[targetSprite] !== undefined &&
      squares[targetSprite].classList.contains("focusedSquare")
    ) {
      //var
      tempSprite = squares[selectedSprite].childNodes[0];
      squares[targetSprite].childNodes[0].remove();
      squares[selectedSprite].childNodes[0].remove();
			squares[targetSprite].appendChild(tempSprite);
      setMoved(targetSprite);
      //check if target-sprite is pawn and color and on last row.
      if(squares[targetSprite].childNodes[0].classList.contains("pawn")
      && (targetSprite > 56 && turn === "black" || targetSprite < 9 && turn === "white") ){
        promote(turn, targetSprite);
      }
      switch (turn) {
        case "white":
          turn = "black";
          opponent = "white";
          break;
        case "black":
          turn = "white";
          opponent = "black";
          break;
      }
      turnIndicator.style.backgroundColor = turn;
    }
    squares.forEach((element) => {
      element.classList.remove("focusedSquare");
    });
    previousPosition = selectedSprite;
    selectedSprite = undefined;
		enPassant = false;
		castle = false;
    return;
  }
  //------------------------------------------------------ select piece/highligh squares below
  if (selectedSprite === undefined) {
    if (
      event.target.classList.contains(turn) &&
      event.target.classList.contains("bishop")
    ) {
      bishopMoves(targetSprite);
    }
    if (
      event.target.classList.contains(turn) &&
      event.target.classList.contains("knight")
    ) {
      knightMoves(targetSprite);
    }
    if (
      event.target.classList.contains(turn) &&
      event.target.classList.contains("rook")
    ) {
      rookMoves(targetSprite);
    }
    if (
      event.target.classList.contains(turn) &&
      event.target.classList.contains("queen")
    ) {
      rookMoves(targetSprite);
      bishopMoves(targetSprite);
    }
    if (
      event.target.classList.contains(turn) &&
      event.target.classList.contains("king")
    ) {
      kingMoves(targetSprite);
      //need function for castling
      check(); //should be run after all moves, and un-toggle all focused squares that put u in check
    }
    //switch for pawn-movement (as they have separate rules for colors)
    //could actually have a function with a switch in stead.
    //would like to return an "en passant" value to somewhere
    switch (turn) {
      case "white":
        //create ifs that run functions in stead of creating monolithic ifs
        //these functions only need to focus correct squares.
        //white pawn moves:
        if (
          event.target.classList.contains("white") &&
          event.target.classList.contains("pawn")
        ) {
          if (squares[targetSprite - 8].childNodes[0] == undefined) {
            //this check could be in a function maybe?
            squares[targetSprite - 8].classList.toggle("focusedSquare");
            if (
							!squares[targetSprite].childNodes[0].classList.contains("moved")&&
							squares[targetSprite - 16].childNodes[0] == undefined 
              //Math.ceil(targetSprite / 8) === 7
            ) {
              squares[targetSprite - 16].classList.toggle("focusedSquare");
            }
          }
          //add to function
          if (
            squares[targetSprite - 7].childNodes[0] != undefined &&
            targetSprite % 8 !== 0 &&
            !squares[targetSprite - 7].childNodes[0].classList.contains("white")
          ) {
            //modulus needs to check if on the end of board?
            squares[targetSprite - 7].classList.toggle("focusedSquare");
          } else if (
            tempSprite.classList.contains("pawn") &&
            previousPosition === tempSprite.parentNode.id.split("-")[1] - 16 &&
            previousPosition === targetSprite - 15
          ) {
            squares[targetSprite - 7].classList.toggle("focusedSquare");
            enPassant = true;
          }
          if (
            squares[targetSprite - 9].childNodes[0] != undefined &&
            targetSprite % 8 !== 1 &&
            !squares[targetSprite - 9].childNodes[0].classList.contains("white")
          ) {
            squares[targetSprite - 9].classList.toggle("focusedSquare");
          } else if (
            tempSprite.classList.contains("pawn") &&
            previousPosition === tempSprite.parentNode.id.split("-")[1] - 16 &&
            previousPosition === targetSprite - 17
          ) {
            squares[targetSprite - 9].classList.toggle("focusedSquare");
            enPassant = true;
          }
        }
        break;
      //black pawn moves:
      case "black":
        if (
          event.target.classList.contains("black") &&
          event.target.classList.contains("pawn")
        ) {
          if (squares[targetSprite - -8].childNodes[0] == undefined) {
            squares[targetSprite - -8].classList.toggle("focusedSquare");
            if (
							!squares[targetSprite].childNodes[0].classList.contains("moved")&&
							squares[targetSprite - -16].childNodes[0] == undefined
              //Math.ceil(targetSprite / 8) === 2
            ) {
              squares[targetSprite - -16].classList.toggle("focusedSquare");
            }
          }
          if (
            squares[targetSprite - -7].childNodes[0] != undefined &&
            targetSprite % 8 !== 1 &&
            !squares[targetSprite - -7].childNodes[0].classList.contains(
              "black"
            )
          ) {
            squares[targetSprite - -7].classList.toggle("focusedSquare");
          } else if (
            tempSprite.classList.contains("pawn") &&
            previousPosition === tempSprite.parentNode.id.split("-")[1] - -16 &&
            previousPosition === targetSprite - -15
          ) {
            squares[targetSprite - -7].classList.toggle("focusedSquare");
            enPassant = true;
          }
          if (
            squares[targetSprite - -9].childNodes[0] != undefined &&
            targetSprite % 8 !== 0 &&
            !squares[targetSprite - -9].childNodes[0].classList.contains(
              "black"
						) &&
						targetSprite - -9 < squares.length
          ) {
            squares[targetSprite - -9].classList.toggle("focusedSquare");
          } else if (
            tempSprite.classList.contains("pawn") &&
            previousPosition === tempSprite.parentNode.id.split("-")[1] - -16 &&
            previousPosition === targetSprite - -17
          ) {
            squares[targetSprite - -9].classList.toggle("focusedSquare");
            enPassant = true;
          }
        }
        break;
    }

    selectedSprite = targetSprite;
    if (document.querySelectorAll(".focusedSquare")[0] === undefined) {
      selectedSprite = undefined;
      enPassant = false;
    }
  }
}

// initializes the chess board
//------------------------------------------------------------------------------------
function createChessBoard() {
  ////add chessStartingSetting when two players on separate devices is implemented
  ////could possibly be in a private connected session
  // if(localStorage.getItem("chessStartingSetting")){
  //     localStorage.setItem("chessStartingSetting", (parseInt(localStorage.getItem("chessStartingSetting"))*(-1)).toString());
  // }
  // else{
  //     localStorage.setItem("chessStartingSetting", "1");
  // }
  turn = "white";
  for (let i = 0; i < 9; i++) {
    var row = document.createElement("tr");
    row.id = i;
    gameTable.appendChild(row);
    for (let j = 1; j <= 9; j++) {
      var square = document.createElement("td");
      if (j === 9) {
        //if's for letters & numbers
        if (i === 8) {
          square.style.backgroundColor = turn;
          square.id = "turnIndicator";
        } else {
          square.innerHTML = i + 1;
          square.style.minWidth = "1em";
          square.style.textAlign = "center";
        }
      } else if (i === 8) {
        square.innerHTML = String.fromCharCode(64 + j);
        square.style.textAlign = "center";
      } else {
        var squareNum = j + i * 8;
        square.className += " crop";
        square.addEventListener("click", highlightMoves);
        if (i % 2 === 0 && squareNum % 2 !== 0) {
          square.classList.add("blueSquare");
          //square.style.backgroundImage = "linear-gradient(to right, #314755 0%, #26a0da  41%, #314755  100%)";
        } else if (i % 2 === 1 && squareNum % 2 !== 1) {
          square.classList.add("blueSquare");
          //square.style.backgroundImage = "linear-gradient(to right, #314755 0%, #26a0da  41%, #314755  100%)";
        } else {
          square.classList.add("brownSquare");
          //square.style.backgroundImage = "linear-gradient(to right, #885456 0%, #a07060  41%, #885456  100%)";
        }
        square.id = `square-${squareNum}`; //change to just squarenum if need be
        //create black pawns: --- just realised this should've been a switch-statement
        if (squareNum > 8 && squareNum < 17) {
          let sprite = document.createElement("img");
          sprite.style.position = "absolute";
          sprite.src = "sprites.png";
          sprite.classList.add("black", "pawn");
          // ------- dynamic styling, possible to implement if we want to make the board more resizable/dynamic.
          // sprite.style.height = `${chessTable.height/6}`;
          // sprite.style.width = `${chessTable.height/6}`;
          // sprite.style.width = "200px"; // size is now halved approx
          // sprite.style.top = "-33px"; //add black class
          // sprite.style.left = `${-33.16*5}px`; //add pawn class that sets this auto in css-file
          // sprite.style.marginLeft = -sprite.width/6;
          // sprite.style.marginTop = -sprite.height/2;
          sprite.style.overflow = "hidden";
          square.appendChild(sprite);
          //sprite.width = chess.png.width;
        } //create black bishops:
        else if (squareNum === 3 || squareNum === 6) {
          let sprite = document.createElement("img");
          sprite.style.position = "absolute";
          sprite.src = "sprites.png";
          sprite.classList.add("black", "bishop");
          sprite.style.overflow = "hidden";
          square.appendChild(sprite);
        } //create black knights
        else if (squareNum === 2 || squareNum === 7) {
          let sprite = document.createElement("img");
          sprite.style.position = "absolute";
          sprite.src = "sprites.png";
          sprite.classList.add("black", "knight");
          sprite.style.overflow = "hidden";
          square.appendChild(sprite);
        } //create black rooks
        else if (squareNum === 1 || squareNum === 8) {
          let sprite = document.createElement("img");
          sprite.style.position = "absolute";
          sprite.src = "sprites.png";
          sprite.classList.add("black", "rook");
          sprite.style.overflow = "hidden";
          square.appendChild(sprite);
        } //create black king
        else if (squareNum === 4) {
          let sprite = document.createElement("img");
          sprite.style.position = "absolute";
          sprite.src = "sprites.png";
          sprite.classList.add("black", "king");
          sprite.style.overflow = "hidden";
          square.appendChild(sprite);
        } //create black queen
        else if (squareNum === 5) {
          let sprite = document.createElement("img");
          sprite.style.position = "absolute";
          sprite.src = "sprites.png";
          sprite.classList.add("black", "queen");
          sprite.style.overflow = "hidden";
          square.appendChild(sprite);
        } //create white pawns:
        else if (squareNum > 48 && squareNum < 57) {
          let sprite = document.createElement("img");
          sprite.style.position = "absolute";
          sprite.src = "sprites.png";
          sprite.classList.add("white", "pawn");
          sprite.style.overflow = "hidden";
          square.appendChild(sprite);
        } //create white bishops:
        else if (squareNum === 59 || squareNum === 62) {
          let sprite = document.createElement("img");
          sprite.style.position = "absolute";
          sprite.src = "sprites.png";
          sprite.classList.add("white", "bishop");
          sprite.style.overflow = "hidden";
          square.appendChild(sprite);
        } //create white knights
        else if (squareNum === 58 || squareNum === 63) {
          let sprite = document.createElement("img");
          sprite.style.position = "absolute";
          sprite.src = "sprites.png";
          sprite.classList.add("white", "knight");
          sprite.style.overflow = "hidden";
          square.appendChild(sprite);
        } //create white rooks
        else if (squareNum === 57 || squareNum === 64) {
          let sprite = document.createElement("img");
          sprite.style.position = "absolute";
          sprite.src = "sprites.png";
          sprite.classList.add("white", "rook");
          sprite.style.overflow = "hidden";
          square.appendChild(sprite);
        } //create white king
        else if (squareNum === 60) {
          let sprite = document.createElement("img");
          sprite.style.position = "absolute";
          sprite.src = "sprites.png";
          sprite.classList.add("white", "king");
          sprite.style.overflow = "hidden";
          square.appendChild(sprite);
        } //create white queen
        else if (squareNum === 61) {
          let sprite = document.createElement("img");
          sprite.style.position = "absolute";
          sprite.src = "sprites.png";
          sprite.classList.add("white", "queen");
          sprite.style.overflow = "hidden";
          square.appendChild(sprite);
        }
        square.style.overflow = "hidden";
        squares[squareNum] = square;
        //row.appendChild(square);
        //adjust td size in css to make all the squares similar, and also then
        //letters and nums will be same size boxes (aligned)
      }
      row.appendChild(square);
    }
  }
  tempSprite = squares[1]; //have to have a "previous move" for some functions (en passant) to work :s
  turnIndicator = document.getElementById("turnIndicator");
  promotionDialog = document.getElementById("promotionDialog");
  promotionForm = document.getElementById("promotionForm");
  promotionDialog.addEventListener('close', function onClose() {
    promotionForm.innerHTML = "";
  });
}

//------------------------------- moves:
//pawn moves elsewhere, queen moves = combination of bishop and rook moves
function bishopMoves(targetBishop) {
  for (let i = targetBishop - 9; i > 0 && i % 8 > 0; i -= 9) {
    if (squares[i].childNodes[0] != undefined) {
      if (squares[i].childNodes[0].classList.contains(opponent)) {
        squares[i].classList.toggle("focusedSquare");
      }
      break;
    } else {
      squares[i].classList.toggle("focusedSquare");
    }
  }
  for (let i = targetBishop - 7; i > 0 && (i - 1) % 8 !== 0; i -= 7) {
    if (squares[i].childNodes[0] != undefined) {
      if (squares[i].childNodes[0].classList.contains(opponent)) {
        squares[i].classList.toggle("focusedSquare");
      }
      break;
    } else {
      squares[i].classList.toggle("focusedSquare");
    }
  }
  for (
    let i = targetBishop + 9;
    i < squares.length && (i - 1) % 8 !== 0;
    i += 9
  ) {
    if (squares[i].childNodes[0] != undefined) {
      if (squares[i].childNodes[0].classList.contains(opponent)) {
        squares[i].classList.toggle("focusedSquare");
      }
      break;
    } else {
      squares[i].classList.toggle("focusedSquare");
    }
  }
  for (let i = targetBishop + 7; i < squares.length && i % 8 > 0; i += 7) {
    if (squares[i].childNodes[0] != undefined) {
      if (squares[i].childNodes[0].classList.contains(opponent)) {
        squares[i].classList.toggle("focusedSquare");
      }
      break;
    } else {
      squares[i].classList.toggle("focusedSquare");
    }
  }
}

function knightMoves(targetKnight) {
  if (
    targetKnight + 17 < squares.length &&
    Math.ceil((targetKnight + 17) / 8) - 2 === Math.ceil(targetKnight / 8)
  ) {
    //(((Math.ceil(targetKnight + 17))/8)-2) === Math.ceil(targetKnight/8)
    if (
      squares[targetKnight + 17].childNodes[0] === undefined ||
      !squares[targetKnight + 17].childNodes[0].classList.contains(turn)
    ) {
      squares[targetKnight + 17].classList.toggle("focusedSquare");
    }
  }
  if (
    targetKnight + 15 < squares.length &&
    Math.ceil((targetKnight + 15) / 8) - 2 === Math.ceil(targetKnight / 8)
  ) {
    if (
      squares[targetKnight + 15].childNodes[0] === undefined ||
      !squares[targetKnight + 15].childNodes[0].classList.contains(turn)
    ) {
      squares[targetKnight + 15].classList.toggle("focusedSquare");
    }
  }
  if (
    targetKnight + 10 < squares.length &&
    Math.ceil((targetKnight + 10) / 8) - 1 === Math.ceil(targetKnight / 8)
  ) {
    if (
      squares[targetKnight + 10].childNodes[0] === undefined ||
      !squares[targetKnight + 10].childNodes[0].classList.contains(turn)
    ) {
      squares[targetKnight + 10].classList.toggle("focusedSquare");
    }
  }
  if (
    targetKnight + 6 < squares.length &&
    Math.ceil((targetKnight + 6) / 8) - 1 === Math.ceil(targetKnight / 8)
  ) {
    if (
      squares[targetKnight + 6].childNodes[0] === undefined ||
      !squares[targetKnight + 6].childNodes[0].classList.contains(turn)
    ) {
      squares[targetKnight + 6].classList.toggle("focusedSquare");
    }
  }
  if (
    targetKnight - 17 > 0 &&
    Math.ceil((targetKnight - 17) / 8) + 2 === Math.ceil(targetKnight / 8)
  ) {
    //(((Math.ceil(targetKnight + 17))/8)-2) === Math.ceil(targetKnight/8)
    if (
      squares[targetKnight - 17].childNodes[0] === undefined ||
      !squares[targetKnight - 17].childNodes[0].classList.contains(turn)
    ) {
      squares[targetKnight - 17].classList.toggle("focusedSquare");
    }
  }
  if (
    targetKnight - 15 > 0 &&
    Math.ceil((targetKnight - 15) / 8) + 2 === Math.ceil(targetKnight / 8)
  ) {
    if (
      squares[targetKnight - 15].childNodes[0] === undefined ||
      !squares[targetKnight - 15].childNodes[0].classList.contains(turn)
    ) {
      squares[targetKnight - 15].classList.toggle("focusedSquare");
    }
  }
  if (
    targetKnight - 10 > 0 &&
    Math.ceil((targetKnight - 10) / 8) + 1 === Math.ceil(targetKnight / 8)
  ) {
    if (
      squares[targetKnight - 10].childNodes[0] === undefined ||
      !squares[targetKnight - 10].childNodes[0].classList.contains(turn)
    ) {
      squares[targetKnight - 10].classList.toggle("focusedSquare");
    }
  }
  if (
    targetKnight - 6 > 0 &&
    Math.ceil((targetKnight - 6) / 8) + 1 === Math.ceil(targetKnight / 8)
  ) {
    if (
      squares[targetKnight - 6].childNodes[0] === undefined ||
      !squares[targetKnight - 6].childNodes[0].classList.contains(turn)
    ) {
      squares[targetKnight - 6].classList.toggle("focusedSquare");
    }
  }
}

function rookMoves(targetRook) {
  for (let i = 1; i <= (targetRook - 1) % 8; i++) {
    if (squares[targetRook - i].childNodes[0] !== undefined) {
      if (!squares[targetRook - i].childNodes[0].classList.contains(turn)) {
        squares[targetRook - i].classList.toggle("focusedSquare");
      }
      break;
    }
    squares[targetRook - i].classList.toggle("focusedSquare");
  }
  for (let i = targetRook + 1; (i - 1) % 8 > 0; i++) {
    if (squares[i].childNodes[0] !== undefined) {
      if (!squares[i].childNodes[0].classList.contains(turn)) {
        squares[i].classList.toggle("focusedSquare");
      }
      break;
    }
    squares[i].classList.toggle("focusedSquare");
  }

  for (let i = targetRook; i > 8; i -= 8) {
    if (squares[i - 8].childNodes[0] !== undefined) {
      if (!squares[i - 8].childNodes[0].classList.contains(turn)) {
        squares[i - 8].classList.toggle("focusedSquare");
      }
      break;
    }
    squares[i - 8].classList.toggle("focusedSquare");
  }

  for (let i = targetRook; i <= 56; i += 8) {
    if (squares[i + 8].childNodes[0] !== undefined) {
      if (!squares[i + 8].childNodes[0].classList.contains(turn)) {
        squares[i + 8].classList.toggle("focusedSquare");
      }
      break;
    }
    squares[i + 8].classList.toggle("focusedSquare");
  }
}

function kingMoves(targetKing) {
	//castling
	if(!squares[targetKing].childNodes[0].classList.contains("moved"))
	{
		if(
			squares[targetKing-3].childNodes[0] !== undefined &&
			!squares[targetKing-3].childNodes[0].classList.contains("moved") &&
			squares[targetKing-2].childNodes[0] === undefined &&
			squares[targetKing-1].childNodes[0] === undefined //need to check check for these squares too
		){
			squares[targetKing-2].classList.toggle("focusedSquare");
			castle = true;
		}
		if(
			squares[targetKing+4].childNodes[0] !== undefined &&
			!squares[targetKing+4].childNodes[0].classList.contains("moved") &&
			squares[targetKing+3].childNodes[0] === undefined &&
			squares[targetKing+2].childNodes[0] === undefined && 
			squares[targetKing+1].childNodes[0] === undefined //check if +1 & +2 are also checked.
		){
			squares[targetKing+2].classList.toggle("focusedSquare");
			castle = true;
		}
	}

	//general king moves
  if (targetKing % 8 !== 1 && targetKing - 1 >= 0) {
    if (squares[targetKing - 1].childNodes[0] !== undefined) {
      if (!squares[targetKing - 1].childNodes[0].classList.contains(turn)) {
        squares[targetKing - 1].classList.toggle("focusedSquare");
      }
    } else {
      squares[targetKing - 1].classList.toggle("focusedSquare");
    }
  }
  if (targetKing % 8 !== 0 && targetKing - 7 >= 0) {
    if (squares[targetKing - 7].childNodes[0] !== undefined) {
      if (!squares[targetKing - 7].childNodes[0].classList.contains(turn)) {
        squares[targetKing - 7].classList.toggle("focusedSquare");
      }
    } else {
      squares[targetKing - 7].classList.toggle("focusedSquare");
    }
  }
  if (targetKing - 8 >= 0) {
    if (squares[targetKing - 8].childNodes[0] !== undefined) {
      if (!squares[targetKing - 8].childNodes[0].classList.contains(turn)) {
        squares[targetKing - 8].classList.toggle("focusedSquare");
      }
    } else {
      squares[targetKing - 8].classList.toggle("focusedSquare");
    }
  }
  if (targetKing % 8 !== 1 && targetKing - 9 >= 0) {
    if (squares[targetKing - 9].childNodes[0] !== undefined) {
      if (!squares[targetKing - 9].childNodes[0].classList.contains(turn)) {
        squares[targetKing - 9].classList.toggle("focusedSquare");
      }
    } else {
      squares[targetKing - 9].classList.toggle("focusedSquare");
    }
  }
  if (squares.length - 1 >= targetKing + 1 && targetKing % 8 !== 0) {
    if (squares[targetKing + 1].childNodes[0] !== undefined) {
      if (!squares[targetKing + 1].childNodes[0].classList.contains(turn)) {
        squares[targetKing + 1].classList.toggle("focusedSquare");
      }
    } else {
      squares[targetKing + 1].classList.toggle("focusedSquare");
    }
  }
  if (squares.length - 1 >= targetKing + 7 && targetKing % 8 !== 1) {
    if (squares[targetKing + 7].childNodes[0] !== undefined) {
      if (!squares[targetKing + 7].childNodes[0].classList.contains(turn)) {
        squares[targetKing + 7].classList.toggle("focusedSquare");
      }
    } else {
      squares[targetKing + 7].classList.toggle("focusedSquare");
    }
  }
  if (squares.length - 1 >= targetKing + 8) {
    if (squares[targetKing + 8].childNodes[0] !== undefined) {
      if (!squares[targetKing + 8].childNodes[0].classList.contains(turn)) {
        squares[targetKing + 8].classList.toggle("focusedSquare");
      }
    } else {
      squares[targetKing + 8].classList.toggle("focusedSquare");
    }
  }
  if (squares.length - 1 >= targetKing + 9 && targetKing % 8 !== 0) {
    if (squares[targetKing + 9].childNodes[0] !== undefined) {
      if (!squares[targetKing + 9].childNodes[0].classList.contains(turn)) {
        squares[targetKing + 9].classList.toggle("focusedSquare");
      }
    } else {
      squares[targetKing + 9].classList.toggle("focusedSquare");
    }
  }
  //waah, can't check himself, so we need extensive checks for that :/
  //need to make a initialize turn to check which pieces cannot be moved --
  //and where the king cannot move
  //we need to make a pawn-threatens-function
}

function setMoved(sprite){ //doesn't work right in chrome
  if(!squares[sprite].childNodes[0].classList.contains("moved")){
    squares[sprite].childNodes[0].classList.add("moved");
  }
}

function promote(turn, sprite)
{
  squares[sprite].childNodes[0].classList.remove("pawn");
  forPromotion = sprite;
  onOpen(turn);
  // squares[sprite].childNodes[0].classList.add(onOpen(turn));
}

function check(player, square) {
	//I wish I made all my pieces have x moves assigned, would be easier to check moves
  //check if any piece can attack square
  //check if player is in check
  //maybe i should make a moved class added to any piece that is moved -
  //helps pawns, kings & towers for en-passant and castling

  //I CAN USE KING CLASS
  //to check if it's targetable by opposite sides pieces
  //remove highlight if move is illegal
  //actually I have to go into each sprite-types individual moves and add rules to make moves illegal
  //LIKE "if this piece moves like this, is allied king in check ? don't highlight move : highlight move"
}

function endTurn() {
  //end turn and run check before moving piece
}


 function onOpen(turn) {
  if (typeof promotionDialog.showModal === "function") {
    for(let ai = 0; ai < promotionOptions.length; ai ++){
      let optionWrapper = document.createElement("div");
      optionWrapper.classList.add("optionWrapper", "crop"); 
      optionWrapper.style.backgroundColor = "#888888";
      let option = document.createElement("img");
      option.style.position = "absolute";
      option.src = "sprites.png";
      option.classList.add(turn, promotionOptions[ai], "moved");
      option.style.overflow = "hidden";
      option.addEventListener("click", function (e) {
        squares[forPromotion].childNodes[0].remove();
        squares[forPromotion].appendChild(e.target);
        promotionDialog.close();
      })
      optionWrapper.appendChild(option);
      //felix LastChanged
      promotionForm.appendChild(optionWrapper);
    }
    promotionDialog.showModal();
  } else {
    alert("The <dialog> API is not supported by this browser");
  }
};