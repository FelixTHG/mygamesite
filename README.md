This gamepage is a personal education project intended to give the maintainer (me!) some hands-on experience with some of the tools available for web-development.

In the future more games will be added. My intention is that each game should use a different tool.

Tools planned for future use:
Vanilla JS: being used for chess
Web-Sockets: will hopefully be used so that players can play online rather than hot-seat
Python: will hopefully be used to create a chess-engine, that can teach itself how to play chess
C#: will hopefully be used to create a backend that will let users have profiles
React: will be used for checkers or maybe Mankala
Vue: undecided
Angular: undecided


I'm also looking to purchase a powerful tower-server/desktop, that is going to be set up as a server to host the games/webapp. It possible that these apps (& other services) will be separated into microservices.

